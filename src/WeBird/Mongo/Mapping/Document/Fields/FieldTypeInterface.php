<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 04.10.14
 * Time: 23:13
 */

namespace WeBird\Mongo\Mapping\Document\Fields;


interface FieldTypeInterface
{

    public function isScalar();

    public function getName();

    public function getType();

}
