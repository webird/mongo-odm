<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 29.09.14
 * Time: 22:24
 */

namespace WeBird\Mongo\Mapping\Document;

interface CollectionBindedDocumentInterface extends BaseDocumentInterface
{

    public static function find($query, $fields = []);

    public static function findOne($query, $fields = []);

    public static function update($query, $set);

    public static function remove($query);

}
