<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 04.10.14
 * Time: 20:24
 */

namespace WeBird\Mongo\Mapping\Document\Traits;

trait MetaWriterTrait
{

    protected function preSaveWriteMetaData()
    {

        $meta = empty($this->_meta) ? [] : $this->_meta;

        $meta['class'] = get_class($this);
        $meta['types'] = $this->_mongo_types;
        $meta['fields'] = $this->_mongo_fields;

        $this->_meta = $meta;

    }

}
