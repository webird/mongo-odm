<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 04.10.14
 * Time: 20:29
 */

namespace WeBird\Mongo\Mapping\Document\Traits;

use WeBird\Mongo\Client\Client;
use WeBird\Mongo\Client\Collection;
use WeBird\Mongo\Mapping\Document\Fields\FieldTypeInterface;
use WeBird\Mongo\Mapping\Mapper\DefaultMapper;
use WeBird\Mongo\Mapping\MappingManager;

trait DocumentTrait
{
    /** @var Collection */
    protected static $_mongo_collection;
    /** @var array */
    protected static $_mongo_fields;
    protected static $_mongo_types;

    protected static $_mongo_configured = false;

    protected static $_mongo_mapper;

    public function __construct()
    {
        static::conf();
    }

    protected static function conf()
    {
        if (static::$_mongo_configured) {
            return;
        }
        static::$_mongo_configured = true;
        static::configure();
    }

    public static function getDatabase()
    {
        static::conf();

        return static::$_mongo_collection->getDataBase();
    }

    public static final function find($query, $fields = [])
    {
        return static::doFind($query, $fields);
    }

    protected static function doFind($query, $fields = [])
    {
        static::conf();
        $ns = (string)static::$_mongo_collection;
        DefaultMapper::addDocumentClass($ns, get_called_class());

        return static::$_mongo_collection->find($query, $fields);
    }

    /**
     * @param array $query
     * @param array $fields
     * @return static
     */
    public static final function findOne($query, $fields = [])
    {
        return static::doFind($query, $fields)->current();
    }

    public final static function remove($query, $limit = 0, $options = ['w' => 1, 'j' => 1])
    {
        self::conf();
        $result = self::doRemove($query, $limit, $options);
        Client::processWriteResults($result, $options);
    }

    protected static function doRemove($query, $limit = 0, $options = [])
    {
        return self::$_mongo_collection->remove($query, $options);
    }

    public final static function update($query, $set, $options = ['w' => 1, 'j' => 1, 'multiple' => 1])
    {
        self::conf();
        $result = self::doUpdate($query, $set, $options);
        Client::processWriteResults($result, $options);

        return $result;
    }

    protected static function doUpdate($query, $set, $options = [])
    {
        return self::$_mongo_collection->update($query, $set, $options);
    }

    protected static function setMongoCollection(Collection $collection)
    {
        static::$_mongo_collection = $collection;
    }

    public final function save()
    {
        self::conf();
        $this->doSave();
    }

    protected function doSave()
    {
        $doc = [];
        foreach ($this as $field => $value) {
            $doc[$field] = $value;
        }

        static::$_mongo_collection->save($doc);

        $this->hydrate($doc);
    }

    public function hydrate($result, MappingManager $mapper = null)
    {
        if (null === $mapper) {
            $mapper = new MappingManager();
        }

        $ns = (string)static::$_mongo_collection;
        foreach ($result as $field => $value) {
            if (is_array($value) && $mapper) {
                if (isset($value[0])) {
                    foreach ($value as $i => $set) {
                        $this->{$field}[$i] = is_array($set) ? $mapper->mapEmbed("{$ns}:{$field}:*", $set, $this->getMetadata()) : $set;
                    }
                } else {
                    $this->$field = $mapper->mapEmbed("{$ns}:{$field}", $value, $this->getMetadata());
                }
            } else {
                $this->$field = $value;
            }
        }
    }

    public function getMetadata()
    {
        return [
            'types' => static::$_mongo_types,
            'class' => get_called_class(),
        ];
    }

    public function delete()
    {
        $this->doDelete();
        unset($this->_id);
        return $this;
    }

    protected function doDelete()
    {
        static::$_mongo_collection->remove(['_id' => $this->_id]);
    }

    public function reload()
    {
        $data = $this->getCollection()->findOne(
            ['_id' => $this->_id],
            [],
            [Collection::FIND_OPTION_NATIVE => true]
        );
        $this->hydrate($data);

        return $this;
    }

    /**
     * @return Collection
     */
    public static function getCollection()
    {
        static::conf();

        return static::$_mongo_collection;
    }

    protected function addField(FieldTypeInterface $type)
    {
        static::$_mongo_fields[$type->getName()] = $type;
        static::$_mongo_types[$type->getName()] = $type->isScalar() ? $type->getType() : $type->extract();
    }


}
