<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 04.10.14
 * Time: 21:49
 */

namespace WeBird\Mongo\Mapping\Document;


use WeBird\Mongo\Mapping\MappingManager;

interface BaseDocumentInterface
{

    public function save();

    public function hydrate($result, MappingManager $mapper);

    public function delete();

    public static function configure();

}
