<?php
namespace WeBird\Mongo\Mapping\Mapper;

interface MapperInterface
{

    /**
     * try to map document
     *
     * @param string $ns document namespace
     * @param array $doc document as array
     * @param array $info document info
     * @return
     */
    public function map($ns, array $doc, array $info = array());

    /**
     * check document
     *
     * @param array $doc document as array
     * @return boolean
     */
    public function isSupportedDocument(array $doc);

}
