<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 04.10.14
 * Time: 20:16
 */

namespace WeBird\Mongo\Mapping\Mapper;

class MetaMapper implements MapperInterface
{

    public function map($ns, array $doc, array $info = array())
    {
        if (empty($doc['_meta']['class'])) {
            return $doc;
        }

        $class = $doc['_meta']['class'];

        if ($class && class_exists($class)) {
            return $class;
        }
        return false;
    }

    /**
     * check document
     *
     * @param array $doc document as array
     * @return boolean
     */
    public function isSupportedDocument(array $doc)
    {
        return !empty($doc['_meta']['class']);
    }
}
