<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 04.10.14
 * Time: 21:18
 */

namespace WeBird\Mongo\Mapping\Mapper;


class EmbedMapper implements MapperInterface
{

    /**
     * try to map document
     *
     * @param string $ns document namespace
     * @param array $doc document as array
     * @param array $info document info
     * @return boolean|string Class name or false
     */
    public function map($ns, array $doc, array $meta = array())
    {
        $parts = explode(':', $ns, 2);
        if (empty($parts[1]) || empty($meta['types'])) {
            return false;
        }

        $path = explode(':', $parts[1]);

        return $this->findPath($path, $meta['types']);
    }

    protected function findPath($path, $types = [])
    {
        $field = array_shift($path);
        return isset($types[$field]) && (is_array($types[$field])) ?
            $this->findPath($path, $types[$field]) :
            ((count($path) == 0 && isset($types[$field])) ? $types[$field] : false);
    }

    /**
     * check document
     *
     * @param array $doc document as array
     * @return boolean
     */
    public function isSupportedDocument(array $doc)
    {
        return empty($doc['_id']);
    }
}
