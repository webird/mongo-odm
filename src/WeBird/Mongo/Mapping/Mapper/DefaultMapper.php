<?php

namespace WeBird\Mongo\Mapping\Mapper;

class DefaultMapper implements MapperInterface
{

    protected static $documents = [];

    public function __construct()
    {

    }

    public static function addDocumentClass($ns, $modelClass)
    {
        self::$documents[$ns] = $modelClass;
    }

    protected function getDocumentClass($ns)
    {
        return isset(self::$documents[$ns]) && class_exists(self::$documents[$ns]) ? self::$documents[$ns] : null;
    }

    /**
     * try to map document
     *
     * @param string $ns document namespace
     * @param array $doc document as array
     * @param array $info document info
     * @return
     */
    public function map($ns, array $doc, array $info = array())
    {
        $class = $this->getDocumentClass($ns);
        return $class ? : false;
    }

    /**
     * check document
     *
     * @param array $doc document as array
     * @return boolean
     */
    public function isSupportedDocument(array $doc)
    {
        return isset($doc['_id']);
    }
}
