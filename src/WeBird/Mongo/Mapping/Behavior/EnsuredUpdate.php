<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 22.08.15
 * Time: 1:10
 */

namespace WeBird\Mongo\Mapping\Behavior;

use WeBird\Mongo\Client\Client;

trait EnsuredUpdate
{

    public function ensuredUpdate($update, $fields = [])
    {
        $criteria = [
            '_id' => $this->_id
        ];

        foreach ($fields as $field) {
            $criteria[$field] = isset($this->$field) ? $this->$field : ['$exists' => false];
        }

        $result = $this->getCollection()->update($criteria, $update, ['w' => 1, 'j' => 1]);
        Client::processWriteResults($result, [Client::WR_THROW_IF_NO_WRITTEN => true]);

        $this->reload();

        return $result;
    }

}
