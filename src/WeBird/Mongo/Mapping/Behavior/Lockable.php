<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 22.08.15
 * Time: 0:07
 */

namespace WeBird\Mongo\Mapping\Behavior;

use WeBird\Mongo\Exception\LockedDocumentException;

/**
 * Class Lockable
 * @package WeBird\Mongo\Mapping\Behavior
 */
trait Lockable
{
    protected $lockUID = null;

    public function waitLock($timeout = null)
    {
        $start = time();

        while ($this->isLocked()) {
            if ($timeout && (time() - $start) > $timeout) {
                throw new LockedDocumentException();
            }
            usleep(150);
        }
    }

    public function isLocked()
    {
        $this->reload();

        return !empty($this->__lock);
    }

    public function lock()
    {
        $this->__lock = true;
        $result = $this->getCollection()->update(
            ['_id' => $this->_id, '$isolated' => 1, '__lock' => ['$exists' => false]],
            ['$set' => ['__lock' => $this->getLockIdentifier()]]
        );

        if (!$result) {
            $this->__lock = false;
            throw new LockedDocumentException();
        }
    }

    protected function getLockIdentifier()
    {
        return $this->lockUID ?:
            (
            $this->lockUID = uniqid('LOCK_'.$this->_id.'__')
            );
    }

    public function unlock($update = [])
    {
        $this->__lock = false;

        $result = $this->getCollection()->update(
            ['_id' => $this->_id, '__lock' => $this->getLockIdentifier(), '$isolated' => 1],
            ['$set' => $update, '$unset' => ['__lock' => null]]
        );

        if (!$result) {
            $this->__lock = false;
            throw new LockedDocumentException();
        }
    }

}
