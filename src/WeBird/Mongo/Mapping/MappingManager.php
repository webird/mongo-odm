<?php

namespace WeBird\Mongo\Mapping;

use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToResultInterface;
use WeBird\Mongo\Mapping\Mapper\MapperInterface;

class MappingManager implements ApplicableToResultInterface
{
    /**
     * @var array|MapperInterface[]
     */
    protected static $mappers = [];

    public static function addMapper(MapperInterface $mapper)
    {
        static::$mappers[] = $mapper;
    }

    public function isApplicable(array $query, array $fields = [])
    {
        return !($fields && !in_array('_id', $fields));
    }

    /**
     * returns the directives, processed by extension
     *
     * @return array
     */
    public function getDirectives()
    {
        return [];
    }

    /**
     * apply extension to result
     *
     * @param array $record DB record after prev. extension
     * @param string $ns results cursor
     * @param array $query Query to collection
     * @return array processed query
     */
    public function applyToResult($result, $ns, array $query, array $options = [])
    {
        if (!is_array($result)) {
            return $result;
        }

        $document = $this->tryToMap($ns, $options, $result);

        return $document ? : $result;
    }

    public function mapEmbed($namespace, $set, $metadata)
    {
        $document = $this->tryToMap($namespace, $metadata, $set);
        return $document ? : $set;
    }

    protected function tryToMap($namespace, $info, $result)
    {
        foreach (static::$mappers as $mapper) {
            if (!$mapper->isSupportedDocument($result)) {
                continue;
            }

            $class = $mapper->map($namespace, $result, $info);
            if (false !== $class) {

                if ($class && class_exists($class) && in_array('WeBird\Mongo\Mapping\Document\BaseDocumentInterface', class_implements($class))) {
                    $obj = new $class();
                    $obj->hydrate($result, $this);
                    return $obj;
                }

            }
        }
        return false;
    }

}

