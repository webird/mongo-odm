<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 19.08.14
 * Time: 1:33
 */

namespace WeBird\Mongo\Silex;

use Silex\Application;
use Silex\ServiceProviderInterface;
use WeBird\Mongo\Client\Client;
use WeBird\Mongo\Client\QueryExtension\Interfaces\QueryExtensionInterface;
use WeBird\Mongo\Client\QueryExtension\Limit;
use WeBird\Mongo\Client\QueryExtension\MongoId;
use WeBird\Mongo\Client\QueryExtension\Offset;
use WeBird\Mongo\Client\QueryExtension\Order;
use WeBird\Mongo\Mapping\Mapper\DefaultMapper;
use WeBird\Mongo\Mapping\Mapper\EmbedMapper;
use WeBird\Mongo\Mapping\Mapper\MapperInterface;
use WeBird\Mongo\Mapping\Mapper\MetaMapper;
use WeBird\Mongo\Mapping\MappingManager;

class Provider implements ServiceProviderInterface
{
    /**
     * DB Connections configuration
     *
     * @var array
     */
    protected $dbconf = [];

    /**
     * @var array|QueryExtensionInterface[]
     */
    protected $customExtensions = [];

    /**
     * @var array|MapperInterface[]
     */
    protected $customMappers = [];

    /**
     * Registers services on the given app.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Application $app An Application instance
     */
    public function register(Application $app)
    {

    }

    /**
     * Bootstraps the application.
     *
     * This method is called after all services are registered
     * and should be used for "dynamic" configuration (whenever
     * a service must be requested).
     */
    public function boot(Application $app)
    {
        $this->dbconf = $app['webird.mongo.dbconf'];

        if (isset($this->dbconf['dsn']) && !is_array($this->dbconf['dsn'])) {
            $this->dbconf['default'] = $this->dbconf;
        }

        $this->customExtensions = $app['webird.mongo.query.extensions'];
        $this->customMappers = isset($app['webird.mongo.mappers']) ? $app['webird.mongo.mappers'] : [];

        $connections = [];

        foreach ($this->dbconf as $key => $conf) {
            if (empty($conf['dsn'])) {
                continue;
            }

            $dbkey = 'webird.mongo' . ($key != 'default' ? ".{$key}" : '');

            Client::add($key, new Client($conf['dsn'], (isset($conf['options']) ? $conf['options'] : array("connect" => true)), (isset($conf['driver_options']) ? $conf['driver_options'] : [])));

            $app[$dbkey] = $app->share(function () use ($key) {
                return Client::get($key);
            });

            $connections[$dbkey] = $app[$dbkey];
        }

        $this->addExtensions($connections);
    }

    /**
     * @param Application $app
     * @param Client[] $connections
     */
    protected function addExtensions(array $connections)
    {
        $extensions = [
            [10, new MongoId()],
            [10, new Order()],
            [20, new Offset()],
            [21, new Limit()],
            [40, new MappingManager()]
        ];

        $mappers = [
            [10, new EmbedMapper()],
            [11, new MetaMapper()],
            [20, new DefaultMapper()],
        ];

        $priority = 30;
        $makeSortable = function ($record) use (&$priority) {

            if (is_object($record)) {
                $priority += 10;
                return [$priority, $record];
            } elseif (is_array($record) && isset($record[1]) && ($record[1] instanceof QueryExtensionInterface || $record[1] instanceof MapperInterface)) {
                list($priority, $extension) = $record;
                return [(int)$priority, $extension];
            }

            return false;
        };

        $doSort = function ($itemA, $itemB) {
            return ($itemA[0] > $itemB[0]) ? 1 : (($itemB[0] > $itemA[0]) ? -1 : 0);
        };

        $extensions += array_filter(array_map($makeSortable, $this->customExtensions));
        $mappers += array_filter(array_map($makeSortable, $this->customMappers));

        usort($extensions, $doSort);
        usort($mappers, $doSort);

        foreach ($mappers as $_mapper) {
            MappingManager::addMapper($_mapper[1]);
        }

        foreach ($connections as $connection) {
            foreach ($extensions as $record) {
                $connection->addQueryExtension($record[1]);
            }
        }


    }

}
