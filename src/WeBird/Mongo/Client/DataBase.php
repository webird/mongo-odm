<?php

namespace WeBird\Mongo\Client;

/**
 * Class DataBase, extends MongoDB
 * @package WeBird\Mongo\Client
 * @inheritdoc
 */
class DataBase extends \MongoDB
{
    /**
     * @var Client
     */
    protected $client;

    function __construct(Client $client, $name)
    {
        $this->client = $client;
        parent::__construct($client, $name);
    }

    public function __get($key)
    {
        return $this->selectCollection($key);
    }

    /**
     * @param string $name
     * @return Collection
     */
    public function selectCollection($name)
    {
        return new Collection($this, $name);
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

}

