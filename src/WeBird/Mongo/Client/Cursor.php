<?php

namespace WeBird\Mongo\Client;

/**
 * Class Cursor, extends MongoCursor
 * @inheritdoc
 * @package WeBird\Mongo\Client
 */
final class Cursor extends \MongoCursor implements \Iterator, \Traversable
{

    private $namespace = null;
    /**
     * @var Client
     */
    private $client = null;

    /**
     * @var Collection
     */
    private $collection = null;

    /**
     * @var Query
     */
    private $query = [];

    /**
     * Create a new cursor
     *
     * @param Client $mongo MongoClient
     * @param Collection $collection MongoCollection
     * @param Query $query query
     */
    public function __construct(Client $mongo, Collection $collection, Query $query)
    {
        parent::__construct(
            $mongo,
            "$collection",
            $query->getMongoQuery($mongo),
            $query->getMongoFields()
        );

        $this->namespace = "$collection";
        $this->client = $mongo;
        $this->collection = $collection;
        $this->query = $query;
        $this->extendSelf();
        $this->rewind();
    }

    private function extendSelf()
    {
        $this->query->extendCursor($this->client, $this);
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        $result = parent::current();

        return $result !== null ? $this->query->extendResult(
            $this->client,
            $this->collection,
            $result
        ) : null;
    }

    /**
     * get all founded records
     * @param null $key
     * @return array
     */
    public function all($key = null)
    {
        $r = array();
        foreach ($this as $doc) {
            if ($key && isset($doc[$key])) {
                $r[$doc[$key]] = $doc;
            } else {
                $r[] = $doc;
            }
        }
        return $r;
    }

}

