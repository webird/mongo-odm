<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 30.08.15
 * Time: 23:34
 */

namespace WeBird\Mongo\Client;

class Query
{
    protected $original_query = [];
    protected $mongo_query = null;

    protected $fields = [];
    protected $options = [];

    public function __construct($query, $fields = [], $options = [])
    {
        $this->original_query = $query;
        $this->fields = $fields;
        $this->options = $options;
    }

    public function getMongoQuery(Client $client)
    {
        return $this->extendQuery($client);
    }

    protected function extendQuery(Client $client)
    {
        $query = $this->original_query;

        $extensions = $client->getQueryExtensions();

        foreach ($extensions as $extension) {
            if (!$extension->isApplicable($query)) {
                continue;
            }
            $query = $extension->applyToQuery($query);
        }

        return $query;
    }

    public function getQuery()
    {
        return $this->original_query;
    }

    public function getMongoFields()
    {
        return $this->fields ?: [];
    }

    public function getMongoOptions()
    {
        return $this->options ?: [];
    }

    public function extendCursor(Client $client, Cursor $cursor)
    {
        $extensions = $client->getCursorExtensions();
        foreach ($extensions as $extension) {
            if (!$extension->isApplicable($this->original_query)) {
                continue;
            }
            $extension->applyToCursor($cursor, $this->original_query);
        }
    }

    public function extendResult(Client $client, Collection $collection, $result)
    {
        if (!empty($this->options[Collection::FIND_OPTION_NATIVE])) {
            return $result;
        }

        $extensions = $client->getResultExtensions();

        foreach ($extensions as $extension) {
            if (!$extension->isApplicable($this->original_query, $this->fields)) {
                continue;
            }
            $result = $extension->applyToResult($result, "$collection", $this->original_query, $this->options);
        }

        return $result;
    }


}
