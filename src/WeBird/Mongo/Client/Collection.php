<?php

namespace WeBird\Mongo\Client;

/**
 * Class Collection, extends MongoCollection
 * @package WeBird\Mongo\Client
 * @inheritdoc
 */
class Collection extends \MongoCollection
{
    const FIND_OPTION_NATIVE = 'wb_native';

    /**
     * @var Client
     */
    private $client = null;

    /**
     * @inheritdoc
     */
    function  __construct(DataBase $db, $name)
    {
        $this->client = $db->getClient();
        parent::__construct($db, $name);
    }

    /**
     * @return DataBase
     */
    public function getDataBase()
    {
        return $this->db;
    }

    /**
     * @inheritdoc
     */
    public function find($query = null, $fields = null, array $options = [])
    {
        $webirdQuery = ($query instanceof Query) ?
            $query :
            new Query($query, $fields ?: [], $options);;

        return new Cursor($this->client, $this, $webirdQuery);
    }

    /**
     * @inheritdoc
     */
    public function findOne($query = null, $fields = null, array $options = [])
    {
        $webirdQuery = ($query instanceof Query) ?
            $query :
            new Query($query, $fields ?: [], $options);;

        return $webirdQuery->extendResult(
            $this->client,
            $this,
            parent::findOne($webirdQuery->getMongoQuery($this->client), $webirdQuery->getMongoFields())
        );

    }

    /**
     * @inheritdoc
     */
    public function findAndModify($query, $update = null, $fields = null, array $options = null)
    {
        $webirdQuery = ($query instanceof Query) ?
            $query :
            new Query($query, $fields ?: [], $options);;

        return $webirdQuery->extendResult(
            $this->client,
            $this,
            parent::findAndModify(
                $webirdQuery->getMongoQuery($this->client),
                $update,
                $webirdQuery->getMongoFields(),
                $webirdQuery->getMongoOptions()
            )
        );

    }

}
