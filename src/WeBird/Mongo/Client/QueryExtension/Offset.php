<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 21.08.14
 * Time: 1:20
 */

namespace WeBird\Mongo\Client\QueryExtension;


use WeBird\Mongo\Client\Cursor;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToCursorInterface;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToQueryInterface;

class Offset implements ApplicableToCursorInterface, ApplicableToQueryInterface
{

    const DIRECTIVE_SKIP = '__skip';
    const DIRECTIVE_OFFEST = '__offset';

    /**
     * apply extension to cursor
     *
     * @param Cursor $cursor
     * @param array $query
     * @return Cursor
     */
    public function applyToCursor(Cursor $cursor, array $query)
    {
        $offset = isset($query[self::DIRECTIVE_SKIP]) ? $query[self::DIRECTIVE_SKIP] : $query[self::DIRECTIVE_OFFEST];
        return $cursor->skip($offset);
    }

    /**
     * apply extension to query
     *
     * @param array $query
     * @return array processed query
     */
    public function applyToQuery(array $query)
    {
        if (!$this->isApplicable($query)) {
            return $query;
        }

        unset($query[self::DIRECTIVE_OFFEST]);
        unset($query[self::DIRECTIVE_SKIP]);
    }

    public function isApplicable(array $query)
    {
        return isset($query[self::DIRECTIVE_OFFEST]) || isset($query[self::DIRECTIVE_SKIP]);
    }

    /**
     * returns the directives, processed by extension
     *
     * @return array
     */
    public function getDirectives()
    {
        return [self::DIRECTIVE_SKIP, self::DIRECTIVE_OFFEST];
    }
}
