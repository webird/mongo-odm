<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 21.08.14
 * Time: 10:25
 */

namespace WeBird\Mongo\Client\QueryExtension;

use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToQueryInterface;

class MongoId implements ApplicableToQueryInterface
{

    public function isApplicable(array $query)
    {
        return true;
    }

    /**
     * returns the directives, processed by extension
     *
     * @return array
     */
    public function getDirectives()
    {
        return ['_id', '$id'];
    }

    /**
     * apply extension to query
     *
     * @param array $query
     * @return array processed query
     */
    public function applyToQuery(array $query)
    {
        array_walk($query, function (&$v, $k) {
            if ($k == '_id' && !($v instanceof \MongoId)) {
                $v = \MongoId::isValid($v) ? new \MongoId($v) : $v;
            } else if (is_array($v) && isset($v['$id'])) {
                $v = new \MongoId($v['$id']);
            }
        });

        return $query;
    }
}
