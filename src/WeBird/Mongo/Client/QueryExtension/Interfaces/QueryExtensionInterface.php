<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 21.08.14
 * Time: 1:22
 */

namespace WeBird\Mongo\Client\QueryExtension\Interfaces;


interface QueryExtensionInterface
{

    public function isApplicable(array $query);

    /**
     * returns the directives, processed by extension
     *
     * @return array
     */
    public function getDirectives();

}
