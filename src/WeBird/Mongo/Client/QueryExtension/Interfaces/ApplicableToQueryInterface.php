<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 21.08.14
 * Time: 1:24
 */

namespace WeBird\Mongo\Client\QueryExtension\Interfaces;


interface ApplicableToQueryInterface extends QueryExtensionInterface
{

    /**
     * apply extension to query
     *
     * @param array $query
     * @return array processed query
     */
    public function applyToQuery(array $query);

}
