<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 21.08.14
 * Time: 1:23
 */

namespace WeBird\Mongo\Client\QueryExtension\Interfaces;


use WeBird\Mongo\Client\Cursor;

interface ApplicableToCursorInterface extends QueryExtensionInterface
{

    /**
     * apply extension to cursor
     *
     * @param Cursor $cursor
     * @param array $query
     */
    public function applyToCursor(Cursor $cursor, array $query);

}
