<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 23.08.14
 * Time: 18:31
 */

namespace WeBird\Mongo\Client\QueryExtension\Interfaces;


interface ApplicableToResultInterface extends QueryExtensionInterface
{

    /**
     * apply extension to result
     *
     * @param mixed $record DB record after prev. extension
     * @param string $cursor results cursor
     * @param array $query Query to collection
     * @return array processed query
     * @return array $options for extending
     */
    public function applyToResult($result, $ns, array $query, array $options = []);

    /**
     * return true, if extension is applicable for query
     *
     * @param array $query
     * @param array $fields
     * @return boolean
     */
    public function isApplicable(array $query, array $fields = []);


}
