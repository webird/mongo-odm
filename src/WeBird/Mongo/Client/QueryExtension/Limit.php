<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 21.08.14
 * Time: 1:20
 */

namespace WeBird\Mongo\Client\QueryExtension;


use WeBird\Mongo\Client\Cursor;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToCursorInterface;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToQueryInterface;

class Limit implements ApplicableToCursorInterface, ApplicableToQueryInterface
{

    const DIRECTIVE_LIMIT = '__limit';

    /**
     * apply extension to cursor
     *
     * @param Cursor $cursor
     * @return Cursor
     */
    public function applyToCursor(Cursor $cursor, array $query)
    {
        $cursor->limit($query[self::DIRECTIVE_LIMIT]);
    }

    /**
     * apply extension to query
     *
     * @param array $query
     * @return array processed query
     */
    public function applyToQuery(array $query)
    {
        if (empty($query[self::DIRECTIVE_LIMIT])) {
            return $query;
        }
        unset($query[self::DIRECTIVE_LIMIT]);

        return $query;
    }

    public function isApplicable(array $query)
    {
        return isset($query[self::DIRECTIVE_LIMIT]);
    }

    /**
     * returns the directives, processed by extension
     *
     * @return array
     */
    public function getDirectives()
    {
        return [self::DIRECTIVE_LIMIT];
    }
}
