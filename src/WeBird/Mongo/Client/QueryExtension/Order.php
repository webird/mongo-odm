<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 21.08.14
 * Time: 1:20
 */

namespace WeBird\Mongo\Client\QueryExtension;


use WeBird\Mongo\Client\Cursor;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToCursorInterface;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToQueryInterface;

class Order implements ApplicableToCursorInterface, ApplicableToQueryInterface
{

    const DIRECTIVE_SORT = '__sort';
    const DIRECTIVE_ORDER = '__order';

    /**
     * apply extension to cursor
     *
     * @param Cursor $cursor
     * @param array $query
     * @return Cursor
     */
    public function applyToCursor(Cursor $cursor, array $query)
    {
        $sort = isset($query[self::DIRECTIVE_SORT]) ? $query[self::DIRECTIVE_SORT] : $query[self::DIRECTIVE_ORDER];

        return $sort ? $cursor->sort($sort) : $cursor;
    }

    /**
     * returns the directives, processed by extension
     *
     * @return array
     */
    public function getDirectives()
    {
        return [self::DIRECTIVE_ORDER, self::DIRECTIVE_SORT];
    }

    /**
     * apply extension to query
     *
     * @param array $query
     * @return array processed query
     */
    public function applyToQuery(array $query)
    {
        if (!$this->isApplicable($query)) {
            return $query;
        }

        unset($query[self::DIRECTIVE_ORDER]);
        unset($query[self::DIRECTIVE_SORT]);

        return $query;
    }

    public function isApplicable(array $query)
    {
        return isset($query[self::DIRECTIVE_SORT]) || isset($query[self::DIRECTIVE_ORDER]);
    }
}
