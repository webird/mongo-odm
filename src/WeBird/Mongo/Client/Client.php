<?php

namespace WeBird\Mongo\Client;

use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToCursorInterface;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToQueryInterface;
use WeBird\Mongo\Client\QueryExtension\Interfaces\ApplicableToResultInterface;
use WeBird\Mongo\Client\QueryExtension\Interfaces\QueryExtensionInterface;
use WeBird\Mongo\Exception\ConnectionNotFoundException;
use WeBird\Mongo\Exception\WriteException;

/**
 * Class Client, extends MongoClient
 * @package WeBird\Mongo\Client
 * @inheritdoc
 */
class Client extends \MongoClient
{
    const WR_THROW_IF_NO_WRITTEN = 'throw_not_written';
    /**
     * @var array|Client[]
     */
    private static $clients = [];
    protected $queryExtensions = [];
    protected $cursorExtensions = [];
    protected $resultExtensions = [];

    /**
     * @inheritdoc
     */
    public function __construct($server = "mongodb://localhost:27017", $options = array("connect" => true), $driver_options = [])
    {
        parent::__construct($server, $options, $driver_options);
    }

    /**
     * @param string $key
     * @param Client $instance
     */
    public static function add($key, Client $instance)
    {
        self::$clients[$key] = $instance;
    }

    /**
     * @param string $key
     * @return Client
     * @throws \WeBird\Mongo\Exception\ConnectionNotFoundException
     */
    public static function get($key)
    {
        if (empty(self::$clients[$key])) {
            throw new ConnectionNotFoundException("Connection '$key' has not been established");
        }

        return self::$clients[$key];
    }

    public static function processWriteResults($result, $options)
    {
        if (empty($result['ok'])) {
            throw new WriteException(
                empty($result['errmsg']) ? null : $result['errmsg'],
                empty($result['err']) ? null : $result['err']
            );
        } elseif (!empty($options[self::WR_THROW_IF_NO_WRITTEN]) && $result['nModified'] < 1) {
            throw new WriteException("Operation didn't executed", WriteException::CODE_NOT_WRITTEN);
        }
    }

    /**
     * @inheritdoc
     */
    public function __get($dbname)
    {
        return $this->selectDB($dbname);
    }

    /**
     * @inheritdoc
     */
    public function selectDB($dbname)
    {
        return new DataBase($this, $dbname);
    }

    /**
     * @inheritdoc
     */
    public function selectCollection($dbname, $collname = null)
    {
        return $this->selectDB($dbname)->selectCollection($collname);
    }

    /**
     * @param QueryExtensionInterface $extension
     */
    public function addQueryExtension(QueryExtensionInterface $extension)
    {

        if ($extension instanceof ApplicableToQueryInterface) {
            $this->queryExtensions[] = $extension;
        }

        if ($extension instanceof ApplicableToCursorInterface) {
            $this->cursorExtensions[] = $extension;
        }

        if ($extension instanceof ApplicableToResultInterface) {
            $this->resultExtensions[] = $extension;
        }

    }

    /**
     * @return ApplicableToQueryInterface[]
     */
    public function getQueryExtensions()
    {
        return $this->queryExtensions;
    }

    /**
     * @return ApplicableToCursorInterface[]
     */
    public function getCursorExtensions()
    {
        return $this->cursorExtensions;
    }

    /**
     * @return ApplicableToResultInterface[]
     */
    public function getResultExtensions()
    {
        return $this->resultExtensions;
    }

    /**
     * Register client for access via static collection
     * @param string $key
     */
    public function register($key)
    {
        self::$clients[$key] = $this;
    }
}


