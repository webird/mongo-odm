<?php
/**
 * Created by PhpStorm.
 * User: mihard
 * Date: 23.08.15
 * Time: 18:59
 */

namespace WeBird\Mongo\Exception;


class WriteException extends \MongoException
{
    const CODE_NOT_WRITTEN = 900;
}
